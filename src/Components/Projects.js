import React, { Component } from 'react';
import logo from '../logo.svg';
import { transformItems } from '../Utils/Commons';

export class Projects extends Component {
  render() {
    const items = [
      { id: 2, seqId: 4, parent: 5, name: "index.tsx" },
      { id: 3, seqId: 3, parent: 1, name: "Sidebar" },
      { id: 4, seqId: 5, parent: 1, name: "Table" },
      { id: 7, seqId: 5, parent: 5, name: "SelectableDropdown.tsx" },
      { id: 5, seqId: 2, parent: 1, name: "AssignmentTable" },
      { id: 1, seqId: 1, parent: null, name: "components" },
      { id: 6, seqId: 2, parent: null, name: "controllers" },
    ];
    
    const finalItems = transformItems(items);

    console.log(finalItems, 'final items');
    
    return (
      <div className="contain1">
        <img src={logo} className="App-logo" alt="logo" />
      </div>
    )
  }
}

export default Projects;
