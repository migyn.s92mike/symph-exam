//Sorting by object key
const comparison = (itemA, itemB) => {
  let comparison = 0;
  if (itemA > itemB) {
    comparison = 1;
  } else if (itemA < itemB) {
    comparison = -1;
  }
  return comparison;
}

//Group Item by object key parent
const groupParents = (items) => [...new Set(items.map(item => item.parent ))]
  .sort((itemA, itemB) => itemA - itemB)
  .filter(parent => parent !== undefined);

//Ordered by object key seqId
const sortObjectSeqId = (itemA, itemB) => comparison(itemA.seqId, itemB.seqId);

//Added depth Object 
const addObjectDepth = (items, depth) => {
  items.forEach(({ id, seqId, parent, name }, ind) => {
    items[ind] = {
      id, 
      seqId, 
      parent,
      depth,
      name
    }
  });
  return items;
}

//Main function
export const transformItems = (items) => {
  const uniqueParents = groupParents(items);
  let output = [];
  let depth = 0;

  uniqueParents.forEach((parent) => {

    const filterItemsByUniqueParents = items.filter(
      item => item.parent === parent 
      && item.name !== undefined 
      && item.id !== undefined
    ).sort(sortObjectSeqId);

    const getIndexOfParent = output.findIndex(item => item.id === parent) + 1;

    if (output.length && getIndexOfParent > 0) {
      depth++;
      addObjectDepth(filterItemsByUniqueParents, depth);
      const parentItem = output.slice(0, getIndexOfParent);
      const remainingItems = output.slice(getIndexOfParent);

      output = [
        ...parentItem,
        ...filterItemsByUniqueParents,
        ...remainingItems
      ];
    }

    if (getIndexOfParent === 0) {
      addObjectDepth(filterItemsByUniqueParents, depth);
      output = [...output, ...filterItemsByUniqueParents];
    }
  });
  return output;
};
