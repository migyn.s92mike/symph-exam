import './App.css';
import './custom.css';
import Header from './Components/Header';
import Footer from './Components/Footer';
import Projects from './Components/Projects';

function App() {
  return (
    <div className="App dark phase1">
      <div className="Container">
        <Header />
        <Projects />
      </div>
      <Footer />
    </div>
  );
}

export default App;
